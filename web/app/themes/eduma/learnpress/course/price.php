<?php
/**
 * Template for displaying the price of a course
 */
learn_press_prevent_access_directly();

do_action( 'learn_press_before_course_price' );

?>
	<div class="course-price">
		<?php do_action( 'learn_press_begin_course_price' ); ?>
		<div class="value <?php echo learn_press_is_free_course( get_the_ID() ) ? 'free-course' : ''; ?>">
			<?php echo learn_press_get_course_price( null, true ); ?>
		</div>
		<?php do_action( 'learn_press_end_course_price' ); ?>
	</div>
<?php do_action( 'learn_press_after_course_price' ); ?>