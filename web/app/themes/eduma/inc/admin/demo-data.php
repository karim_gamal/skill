<?php

defined( 'DS' ) OR define( 'DS', DIRECTORY_SEPARATOR );

$demo_datas_dir = THIM_DIR . 'inc' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'data';

$demo_datas = array(
	'demo-01' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-01',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-01/demo-01.jpg',
		'title'         => esc_html__( 'Demo 01', 'eduma' )
	),
	'demo-02' => array(
			'data_dir'      => $demo_datas_dir . DS . 'demo-02',
			'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-02/demo-02.jpg',
			'title'         => esc_html__( 'Demo 02', 'eduma' )
	),
	'demo-03' => array(
			'data_dir'      => $demo_datas_dir . DS . 'demo-03',
			'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-03/demo-03.jpg',
			'title'         => esc_html__( 'Demo 03', 'eduma' )
	),
	'demo-04' => array(
			'data_dir'      => $demo_datas_dir . DS . 'demo-04',
			'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-04/demo-04.jpg',
			'title'         => esc_html__( 'Demo 04', 'eduma' )
	),
	'demo-05' => array(
			'data_dir'      => $demo_datas_dir . DS . 'demo-05',
			'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-05/demo-05.jpg',
			'title'         => esc_html__( 'Demo 05', 'eduma' )
	),
);