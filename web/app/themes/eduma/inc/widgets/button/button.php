<?php
/**
 * Widget Name: Button.
 * Author: ThimPress.
 */

class Thim_Button_Widget extends Thim_Widget {

	function __construct() {
		parent::__construct(
			'button',
			esc_html__( 'Thim: Button', 'eduma' ),
			array(
				'description' => esc_html__( 'Add Button', 'eduma' ),
				'help'        => '',
				'panels_groups' => array('thim_widget_group'),
				'panels_icon' => 'dashicons dashicons-welcome-learn-more'
			),
			array(),
			array(
				'title'     => array(
					"type"    => "text",
					"default" => esc_html__("READ MORE", 'eduma'),
					"label"   => esc_html__( "Button Text", 'eduma' ),
				),
				'url'     => array(
					"type"    => "text",
					"default" => "#",
					"label"   => esc_html__( "Destination URL", 'eduma' ),
				),
				'new_window'     => array(
					"type"    => "checkbox",
					"default" => false,
					"label"   => esc_html__( "Open in New Window", 'eduma' ),
				),
				'icon' => array(
					'type'   => 'section',
					'label'  => esc_html__( 'Icon', 'eduma' ),
					'hide'   => true,
					'fields' => array(
						'icon'      => array(
							"type"        => "icon",
							"class"       => "",
							"label"       => esc_html__( "Select Icon:", 'eduma' ),
							"description" => esc_html__( "Select the icon from the list.", 'eduma' ),
							"class_name"  => 'font-awesome',
						),
						// Resize the icon
						'icon_size' => array(
							"type"        => "number",
							"class"       => "",
							"label"       => esc_html__( "Icon Size ", 'eduma' ),
							"suffix"      => "px",
							"default"     => "14",
							"description" => esc_html__( "Select the icon font size.", 'eduma' ),
							"class_name"  => 'font-awesome'
						),
					),
				),
				'layout'        => array(
					'type'   => 'section',
					'label'  => esc_html__( 'Layout', 'eduma' ),
					'hide'   => true,
					'fields' => array(
						'button_size'          => array(
							"type"        => "select",
							"class"       => "",
							"label"       => esc_html__( "Button Size", 'eduma' ),
							"options"     => array(
								"normal" => esc_html__("Normal", 'eduma'),
								"medium"       => esc_html__("Medium", 'eduma'),
								"large"       => esc_html__("Large", 'eduma'),
							),
						),
						'rounding'          => array(
							"type"        => "select",
							"class"       => "",
							"label"       => esc_html__( "Rounding", 'eduma' ),
							"options"     => array(
								"" => esc_html__("None", 'eduma'),
								"very-rounded"       => esc_html__("Very Rounded", 'eduma'),
							),
						),
					)
				),

			),
			THIM_DIR . 'inc/widgets/icon-box/'
		);
	}

	/**
	 * Initialize the CTA widget
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}
}
function thim_button_register_widget() {
	register_widget( 'Thim_Button_Widget' );
}

add_action( 'widgets_init', 'thim_button_register_widget' );