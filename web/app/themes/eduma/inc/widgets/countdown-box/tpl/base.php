<?php

if ( $instance['time_year'] <> '' ) {
	$year = ( (int)($instance['time_year']) != '') ? (int)($instance['time_year']) : date( "Y",  time() );
}
if ( $instance['time_month'] <> '' ) {
	$month = ( (int)($instance['time_month']) != '') ? (int)($instance['time_month']) : date( "m",  time() );
}
if ( $instance['time_day'] <> '' ) {
	$day = ( (int)($instance['time_day']) != '') ? (int)($instance['time_day']) : date( "d",  time() );
}
if ( $instance['time_hour'] <> '' ) {
	$hour = ( (int)($instance['time_hour']) != '') ? (int)($instance['time_hour']) : date( "G",  time() );
}
$style_color = 'color-white';
if ( $instance['style_color'] <> '' ){
	$style_color = 'color-'.$instance['style_color'];
}
$text_align = '';
if ( $instance['text_align'] <> '' ){
	$text_align = $instance['text_align'];
}
$id = uniqid();
echo '<div class="'.$text_align.' '.$style_color.'" id="coming-soon-counter'.$id.'"></div>';

?>
<script type="text/javascript">
<?php echo '
		 jQuery(function () {
		        jQuery(document).ready( function() {
					jQuery("#coming-soon-counter'.$id.'").mbComingsoon({ expiryDate:  new Date(' .$year. ', ' .($month - 1). ', ' . $day . ', ' . $hour .'), speed:100 });
					setTimeout(function () {
						jQuery(window).resize();
					}, 200);
				});
			});
		 '
 ?>
</script>