<?php
$show_count   = $instance['list-options']['show_counts'];
$hierarchical = $instance['list-options']['hierarchical'];
$taxonomy     = 'course_category';

$args_cat = array(
	'show_count'   => $show_count,
	'hierarchical' => $hierarchical,
	'taxonomy'     => $taxonomy,
	'title_li'     => '',
	'current_category' => 1
);
?>
<?php if ( $instance['title'] ) {
	echo ent2ncr($args['before_title'] . $instance['title'] . $args['after_title']);
} ?>
<ul><?php wp_list_categories( $args_cat ); ?> </ul>