<ul class="nav navbar-nav">
	<?php
	if ( has_nav_menu( 'primary' ) ) {
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'container'      => false,
			'items_wrap'     => '%3$s'
		) );
	} else {
		wp_nav_menu( array(
			'theme_location' => '',
			'container'      => false,
			'items_wrap'     => '%3$s'
		) );
	}
	?>
</ul>

<?php
echo '<div class="thim-mobile-login">';
	if ( !is_user_logged_in() ) {
	echo '<div class="thim-link-login">
		<a href="'.esc_url(add_query_arg( 'action', 'register', thim_get_login_page_url() )).'">' . esc_html__('Register','eduma') . '</a>
		<a href="'.esc_url(thim_get_login_page_url()) . '">' . esc_html__('Login','eduma') . '</a>
	</div>';
	} else {
	echo '<div class="thim-logout"><a href="' . esc_url(wp_logout_url(thim_get_login_page_url())) . '">' . esc_html__('Logout','eduma') . '</a></div>';
	}
echo '</div>';
?>