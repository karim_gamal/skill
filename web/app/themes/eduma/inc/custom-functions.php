<?php

/**
 * Animation
 *
 * @param $css_animation
 *
 * @return string
 */
function thim_getCSSAnimation( $css_animation ) {
	$output = '';
	if ( $css_animation != '' ) {
		wp_enqueue_script( 'thim-waypoints' );
		$output = ' wpb_animate_when_almost_visible wpb_' . $css_animation;
	}

	return $output;
}

/**
 * Custom excerpt
 *
 * @param $limit
 *
 * @return array|mixed|string|void
 */
function thim_excerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode( " ", $excerpt ) . '...';
	} else {
		$excerpt = implode( " ", $excerpt );
	}
	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );

	return '<p>' . $excerpt . '</p>';
}

/**
 * Display breadcrumbs
 */
function thim_breadcrumbs() {

	// Do not display on the homepage
	if ( is_front_page() || is_404() ) {
		return;
	}

	// Get the query & post information
	global $post;
	$categories = get_the_category();

	// Build the breadcrums
	echo '<ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" class="breadcrumbs">';


	// Home page
	echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_html( get_home_url() ) . '" title="' . esc_attr__( 'Home', 'eduma' ) . '"><span itemprop="name">' . esc_html__( 'Home', 'eduma' ) . '</span></a></li>';

	if ( is_home() ) {
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_title() ) . '">' . esc_html__( 'Blog', 'eduma' ) . '</span></li>';
	}

	if ( is_single() ) {
		if ( get_post_type() == "tp_event" ) {
			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_post_type_archive_link( 'tp_event' ) ) . '" title="' . esc_html( 'Events', 'eduma' ) . '"><span itemprop="name">' . esc_html( 'Events', 'eduma' ) . '</span></a></li>';
		}
		// Single post (Only display the first category)
		if ( isset( $categories[0] ) ) {
			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" title="' . esc_attr( $categories[0]->cat_name ) . '"><span itemprop="name">' . esc_html( $categories[0]->cat_name ) . '</span></a></li>';
		}
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_title() ) . '">' . esc_html( get_the_title() ) . '</span></li>';

	} else if ( is_category() ) {

		// Category page
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . esc_html( $categories[0]->cat_name ) . '</span></li>';

	} else if ( is_page() ) {

		// Standard page
		if ( $post->post_parent ) {

			// If child page, get parents
			$anc = get_post_ancestors( $post->ID );

			// Get parents in the right order
			$anc = array_reverse( $anc );

			// Parent page loop
			foreach ( $anc as $ancestor ) {
				echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_permalink( $ancestor ) ) . '" title="' . esc_attr( get_the_title( $ancestor ) ) . '"><span itemprop="name">' . esc_html( get_the_title( $ancestor ) ) . '</span></a></li>';
			}
		}

		// Current page
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_title() ) . '"> ' . esc_html( get_the_title() ) . '</span></li>';


	} else if ( is_tag() ) {

		// Display the tag name
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( single_term_title( '', false ) ) . '">' . esc_html( single_term_title( '', false ) ) . '</span></li>';

	} elseif ( is_day() ) {

		// Day archive

		// Year link
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . esc_attr( get_the_time( 'Y' ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'Y' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></a></li>';

		// Month link
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) ) . '" title="' . esc_attr( get_the_time( 'M' ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'M' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></a></li>';

		// Day display
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_time( 'jS' ) ) . '"> ' . esc_html( get_the_time( 'jS' ) ) . ' ' . esc_html( get_the_time( 'M' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></li>';

	} else if ( is_month() ) {

		// Month Archive

		// Year link
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . esc_attr( get_the_time( 'Y' ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'Y' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></a></li>';

		// Month display
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_time( 'M' ) ) . '">' . esc_html( get_the_time( 'M' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></li>';

	} else if ( is_year() ) {

		// Display year archive
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( get_the_time( 'Y' ) ) . '">' . esc_html( get_the_time( 'Y' ) ) . ' ' . esc_html__( 'Archives', 'eduma' ) . '</span></li>';

	} else if ( is_author() ) {

		// Auhor archive

		// Get the author information
		global $author;
		$userdata = get_userdata( $author );

		// Display author name
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr( $userdata->display_name ) . '">' . esc_attr__( 'Author', 'eduma' ) . ' ' . esc_html( $userdata->display_name ) . '</span></li>';

	} else if ( get_query_var( 'paged' ) ) {

		// Paginated archives
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr__( 'Page', 'eduma' ) . ' ' . get_query_var( 'paged' ) . '">' . esc_html__( 'Page', 'eduma' ) . ' ' . esc_html( get_query_var( 'paged' ) ) . '</span></li>';

	} else if ( is_search() ) {

		// Search results page
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr__( 'Search results for:', 'eduma' ) . ' ' . esc_attr( get_search_query() ) . '">' . esc_html__( 'Search results for:', 'eduma' ) . ' ' . esc_html( get_search_query() ) . '</span></li>';

	} elseif ( is_404() ) {
		// 404 page
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr__( '404 Page', 'eduma' ) . '">' . esc_html__( '404 Page', 'eduma' ) . '</span></li>';
	} elseif ( is_archive() ) {
		if ( get_post_type() == "tp_event" ) {
			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . esc_attr__( 'Events', 'eduma' ) . '">' . esc_html__( 'Events', 'eduma' ) . '</span></li>';
		}
	}

	echo '</ul>';
}

/**
 * Get related posts
 *
 * @param     $post_id
 * @param int $number_posts
 *
 * @return WP_Query
 */
function thim_get_related_posts( $post_id, $number_posts = - 1 ) {
	$query = new WP_Query();
	$args  = '';
	if ( $number_posts == 0 ) {
		return $query;
	}
	$args  = wp_parse_args( $args, array(
		'posts_per_page'      => $number_posts,
		'post__not_in'        => array( $post_id ),
		'ignore_sticky_posts' => 0,
		'meta_key'            => '_thumbnail_id',
		'category__in'        => wp_get_post_categories( $post_id )
	) );
	$query = new WP_Query( $args );

	return $query;
}

// bbPress
function thim_use_bbpress() {
	if ( function_exists( 'is_bbpress' ) ) {
		return is_bbpress();
	} else {
		return false;
	}
}

/************ List Comment ***************/
if ( ! function_exists( 'thim_comment' ) ) {
	function thim_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		//extract( $args, EXTR_SKIP );
		if ( 'div' == $args['style'] ) {
			$tag       = 'div';
			$add_below = 'comment';
		} else {
			$tag       = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo ent2ncr( $tag . ' ' ) ?><?php comment_class( 'description_comment' ) ?> id="comment-<?php comment_ID() ?>">
		<div class="wrapper-comment">
			<?php
			if ( $args['avatar_size'] != 0 ) {
				echo '<div class="avatar">';
				echo get_avatar( $comment, $args['avatar_size'] );
				echo '</div>';
			}
			?>
			<div class="comment-right">
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'eduma' ) ?></em>
				<?php endif; ?>

				<div class="comment-extra-info">
					<div
						class="author"><span class="author-name"><?php echo get_comment_author_link(); ?></span></div>
					<div class="date" itemprop="commentTime">
						<?php printf( get_comment_date(), get_comment_time() ) ?></div>
					<?php edit_comment_link( esc_html__( 'Edit', 'eduma' ), '', '' ); ?>

					<?php comment_reply_link( array_merge( $args, array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth']
					) ) ) ?>
				</div>

				<div class="content-comment">
					<?php comment_text() ?>
				</div>
			</div>
		</div>
		<?php
	}
}

// dislay setting layout
require THIM_DIR . 'inc/wrapper-before-after.php';

/**
 * @param $mtb_setting
 *
 * @return mixed
 */
function thim_mtb_setting_after_created( $mtb_setting ) {
	$mtb_setting->removeOption( array( 11 ) );
	$option_name_space = $mtb_setting->owner->optionNamespace;

	$settings   = array(
		'name'      => esc_html__( 'Color Sub Title', 'eduma' ),
		'id'        => 'mtb_color_sub_title',
		'type'      => 'color-opacity',
		'desc'      => ' ',
		'row_class' => 'child_of_' . $option_name_space . '_mtb_using_custom_heading thim_sub_option',
	);
	$settings_1 = array(
		'name' => esc_html__( 'No Padding Content', 'eduma' ),
		'id'   => 'mtb_no_padding',
		'type' => 'checkbox',
		'desc' => ' ',
	);

	$mtb_setting->insertOptionBefore( $settings, 11 );
	$mtb_setting->insertOptionBefore( $settings_1, 16 );

	return $mtb_setting;
}

add_filter( 'thim_mtb_setting_after_created', 'thim_mtb_setting_after_created', 10, 2 );


/**
 * @param $tabs
 *
 * @return array
 */
function thim_widget_group( $tabs ) {
	$tabs[] = array(
		'title'  => esc_html__( 'Thim Widget', 'eduma' ),
		'filter' => array(
			'groups' => array( 'thim_widget_group' )
		)
	);

	return $tabs;
}

add_filter( 'siteorigin_panels_widget_dialog_tabs', 'thim_widget_group', 19 );

/**
 * @param $fields
 *
 * @return mixed
 */
function thim_row_style_fields( $fields ) {
	$fields['parallax'] = array(
		'name'        => esc_html__( 'Parallax', 'eduma' ),
		'type'        => 'checkbox',
		'group'       => 'design',
		'description' => esc_html__( 'If enabled, the background image will have a parallax effect.', 'eduma' ),
		'priority'    => 8,
	);

	return $fields;
}

add_filter( 'siteorigin_panels_row_style_fields', 'thim_row_style_fields' );

/**
 * @param $attributes
 * @param $args
 *
 * @return mixed
 */
function thim_row_style_attributes( $attributes, $args ) {
	if ( ! empty( $args['parallax'] ) ) {
		array_push( $attributes['class'], 'article__parallax' );
	}

	if ( ! empty( $args['row_stretch'] ) && $args['row_stretch'] == 'full-stretched' ) {
		array_push( $attributes['class'], 'thim-fix-stretched' );
	}

	return $attributes;
}

add_filter( 'siteorigin_panels_row_style_attributes', 'thim_row_style_attributes', 10, 2 );

/**
 * @return string
 */
function thim_excerpt_length() {
	$theme_options_data = thim_options_data();
	if ( isset( $theme_options_data['thim_archive_excerpt_length'] ) ) {
		$length = $theme_options_data['thim_archive_excerpt_length'];
	} else {
		$length = '50';
	}

	return $length;
}

add_filter( 'excerpt_length', 'thim_excerpt_length', 999 );

/**
 * @param $text
 *
 * @return mixed|string|void
 */
function thim_wp_new_excerpt( $text ) {
	if ( $text == '' ) {
		$text           = get_the_content( '' );
		$text           = strip_shortcodes( $text );
		$text           = apply_filters( 'the_content', $text );
		$text           = str_replace( ']]>', ']]>', $text );
		$text           = strip_tags( $text );
		$text           = nl2br( $text );
		$excerpt_length = apply_filters( 'excerpt_length', 55 );
		$words          = explode( ' ', $text, $excerpt_length + 1 );
		if ( count( $words ) > $excerpt_length ) {
			array_pop( $words );
			array_push( $words, '' );
			$text = implode( ' ', $words );
		}
	}

	return $text;
}

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
add_filter( 'get_the_excerpt', 'thim_wp_new_excerpt' );

/**
 * Social sharing
 */
function thim_social_share() {
	$theme_options_data = thim_options_data();

	$facebook  = isset( $theme_options_data['thim_sharing_facebook'] ) && $theme_options_data['thim_sharing_facebook'] ? $theme_options_data['thim_sharing_facebook'] : null;
	$twitter   = isset( $theme_options_data['thim_sharing_twitter'] ) && $theme_options_data['thim_sharing_twitter'] ? $theme_options_data['thim_sharing_twitter'] : null;
	$pinterest = isset( $theme_options_data['thim_sharing_pinterest'] ) && $theme_options_data['thim_sharing_pinterest'] ? $theme_options_data['thim_sharing_pinterest'] : null;
	$google    = isset( $theme_options_data['thim_sharing_google'] ) && $theme_options_data['thim_sharing_google'] ? $theme_options_data['thim_sharing_google'] : null;

	if ( $facebook || $twitter || $pinterest || $google ) {
		echo '<ul class="thim-social-share">';
		if ( $facebook ) {

			echo '<li class="facebook">
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, \'script\', \'facebook-jssdk\'));</script>
						<div class="fb-share-button" data-href="' . esc_url( get_the_permalink() ) . '" data-layout="button_count"></div>
					</li>';
		}
		if ( $google ) {
			echo '<li class="google-plus">
						<script src="' . esc_url( "https://apis.google.com/js/platform.js" ) . '" async defer></script>
						<div class="g-plusone" data-width="200"></div>
					</li>';
		}
		if ( $twitter ) {
			echo '<li class="twitter">
						<a href="' . esc_url( get_permalink() ) . '" class="twitter-share-button">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
					</li>';
		}

		if ( $pinterest ) {
			echo '<li class="pinterest">
						<a data-pin-do="buttonBookmark"  href="' . esc_url( "//www.pinterest.com/pin/create/button/" ) . '"><img src="' . esc_url( "//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" ) . '" alt="' . esc_html__( "Pinterest", "eduma" ) . '"/></a>
						<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
					</li>';
		}

		echo '</ul>';
	}

}

add_action( 'thim_social_share', 'thim_social_share' );


/**
 * Display favicon
 */
function thim_favicon() {
	if ( function_exists( 'wp_site_icon' ) ) {
		if ( function_exists( 'has_site_icon' ) ) {
			if ( ! has_site_icon() ) {
				// Icon default
				$thim_favicon_src = get_template_directory_uri() . "/images/favicon.png";
				echo '<link rel="shortcut icon" href="' . esc_url( $thim_favicon_src ) . '" type="image/x-icon" />';

				return;
			}

			return;
		}
	}

	/**
	 * Support WordPress < 4.3
	 */
	$theme_options_data = thim_options_data();
	$thim_favicon_src   = '';
	if ( isset( $theme_options_data['thim_favicon'] ) ) {
		$thim_favicon       = $theme_options_data['thim_favicon'];
		$favicon_attachment = wp_get_attachment_image_src( $thim_favicon, 'full' );
		$thim_favicon_src   = $favicon_attachment[0];
	}
	if ( ! $thim_favicon_src ) {
		$thim_favicon_src = get_template_directory_uri() . "/images/favicon.png";
	}
	echo '<link rel="shortcut icon" href="' . esc_url( $thim_favicon_src ) . '" type="image/x-icon" />';
}

add_action( 'wp_head', 'thim_favicon' );

/**
 * Redirect to custom login page
 */
function thim_login_failed() {
	wp_redirect( add_query_arg( 'result', 'failed', thim_get_login_page_url() ) );
	exit;
}

add_action( 'wp_login_failed', 'thim_login_failed', 1000 );

/**
 * Redirect to custom login page
 *
 * @param $user
 * @param $username
 * @param $password
 */
function thim_verify_username_password( $user, $username, $password ) {

	global $wpdb;
	$page = $wpdb->get_col(
		$wpdb->prepare(
			"SELECT p.ID FROM $wpdb->posts AS p INNER JOIN $wpdb->postmeta AS pm ON p.ID = pm.post_id
			WHERE 	pm.meta_key = %s
			AND 	pm.meta_value = %s
			AND		p.post_type = %s
			AND		p.post_status = %s",
			'thim_login_page',
			'1',
			'page',
			'publish'
		)
	);
	if ( empty( $page[0] ) || ! thim_plugin_active( 'siteorigin-panels/siteorigin-panels.php' ) ) {
		return $user;
	} else {
		$url = null;
		if ( $username == '' && $password == '' ) {
			$url = ( add_query_arg( 'result', 'empty', thim_get_login_page_url() ) );
		} elseif ( $username == '' || $password == '' ) {
			$url = ( add_query_arg( 'result', 'failed', thim_get_login_page_url() ) );
		}
		if ( $url ) {
			if ( ! empty( $_REQUEST['redirect_to'] ) ) {
				$url = add_query_arg( 'redirect_to', urlencode( $_REQUEST['redirect_to'] ), $url );
			}
			wp_redirect( $url );
		}
	}
}

add_filter( 'authenticate', 'thim_verify_username_password', 1, 3 );

/**
 * Filter register link
 *
 * @param $register_url
 *
 * @return string|void
 */
function thim_register_url( $url ) {
	$url = add_query_arg( 'action', 'register', thim_get_login_page_url() );

	return $url;
}

add_filter( 'register_url', 'thim_register_url' );

/**
 * Register failed
 *
 * @param $sanitized_user_login
 * @param $user_email
 * @param $errors
 */
function thim_register_failed( $sanitized_user_login, $user_email, $errors ) {

	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );

	if ( $errors->get_error_code() ) {

		//setup your custom URL for redirection
		$url = add_query_arg( 'action', 'register', thim_get_login_page_url() );

		foreach ( $errors->errors as $e => $m ) {
			$url = add_query_arg( $e, '1', $url );
		}
		wp_redirect( $url );
		exit;
	}
}

add_action( 'register_post', 'thim_register_failed', 99, 3 );

/**
 * Redirect to custom register page in case multi sites
 *
 * @param $url
 *
 * @return mixed
 */
function thim_multisite_register_redirect( $url ) {

	if ( is_multisite() ) {
		$url = add_query_arg( 'action', 'register', thim_get_login_page_url() );
	}

	$user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : '';
	$user_email = isset( $_POST['user_email'] ) ? $_POST['user_email'] : '';
	$errors     = register_new_user( $user_login, $user_email );
	if ( ! is_wp_error( $errors ) ) {
		$redirect_to = ! empty( $_POST['redirect_to'] ) ? $_POST['redirect_to'] : 'wp-login.php?checkemail=registered';
		wp_safe_redirect( $redirect_to );
		exit();
	}

	return $url;
}

add_filter( 'wp_signup_location', 'thim_multisite_register_redirect' );


function thim_multisite_signup_redirect() {
	if ( is_multisite() ) {
		wp_redirect( wp_registration_url() );
		die();
	}
}

add_action( 'signup_header', 'thim_multisite_signup_redirect' );


/**
 * Filter lost password link
 *
 * @param $url
 *
 * @return string
 */
function thim_lost_password_url( $url ) {
	$url = add_query_arg( 'action', 'lostpassword', thim_get_login_page_url() );

	return $url;
}

add_filter( 'lostpassword_url', 'thim_lost_password_url', 99 );


/**
 * Add lost password link into login form
 *
 * @param $content
 * @param $args
 *
 * @return string
 */
function thim_add_lost_password_link( $content, $args ) {
	$content = '<a href="' . wp_lostpassword_url() . '" title="' . esc_attr__( 'Lost Password', 'eduma' ) . '">' . esc_html__( 'Lost your password?', 'eduma' ) . '</a>';

	return $content;
}

add_filter( 'login_form_middle', 'thim_add_lost_password_link', 99 );

/**
 * Register failed
 */
function thim_reset_password_failed() {

	//setup your custom URL for redirection
	$url = add_query_arg( 'action', 'lostpassword', thim_get_login_page_url() );

	if ( empty( $_POST['user_login'] ) ) {
		$url = add_query_arg( 'empty', '1', $url );
		wp_redirect( $url );
		exit;
	} elseif ( strpos( $_POST['user_login'], '@' ) ) {
		$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
		if ( empty( $user_data ) ) {
			$url = add_query_arg( 'user_not_exist', '1', $url );
			wp_redirect( $url );
			exit;
		}
	} elseif ( ! username_exists( $_POST['user_login'] ) ) {
		$url = add_query_arg( 'user_not_exist', '1', $url );
		wp_redirect( $url );
		exit;
	}


}

add_action( 'lostpassword_post', 'thim_reset_password_failed', 99 );

/**
 * Get login page url
 *
 * @return false|string
 */
function thim_get_login_page_url() {
	global $wpdb;
	$page = $wpdb->get_col(
		$wpdb->prepare(
			"SELECT p.ID FROM $wpdb->posts AS p INNER JOIN $wpdb->postmeta AS pm ON p.ID = pm.post_id
			WHERE 	pm.meta_key = %s
			AND 	pm.meta_value = %s
			AND		p.post_type = %s
			AND		p.post_status = %s",
			'thim_login_page',
			'1',
			'page',
			'publish'
		)
	);
	if ( empty( $page[0] ) || ! thim_plugin_active( 'siteorigin-panels/siteorigin-panels.php' ) ) {
		return wp_login_url();
	} else {
		return get_permalink( $page[0] );
	}
}

/**
 * Display feature image
 *
 * @param $attachment_id
 * @param $size_type
 * @param $width
 * @param $height
 * @param $alt
 * @param $title
 *
 * @return string
 */
function thim_get_feature_image( $attachment_id, $size_type = null, $width = null, $height = null, $alt = null, $title = null ) {

	if ( ! $size_type ) {
		$size_type = 'full';
	}
	$src = wp_get_attachment_image_src( $attachment_id, $size_type );
	$style = '';
	if ( ! $src ) {
		// Get demo image
		global $wpdb;
		$attachment_id = $wpdb->get_col(
			$wpdb->prepare(
				"SELECT p.ID FROM $wpdb->posts AS p INNER JOIN $wpdb->postmeta AS pm ON p.ID = pm.post_id
				WHERE 	pm.meta_key = %s
				AND 	pm.meta_value LIKE %s",
				'_wp_attached_file',
				'%demo_image.jpg'
			)
		);

		if ( empty( $attachment_id[0] ) ) {
			return;
		}

		$attachment_id = $attachment_id[0];
		$src           = wp_get_attachment_image_src( $attachment_id, 'full' );

	}

	if ( $width && $height ) {

		if ( $src[1] >= $width || $src[2] >= $height ) {

			$crop = ( $src[1] >= $width && $src[2] >= $height ) ? true : false;

			$src[0] = aq_resize( $src[0], $width, $height, $crop );

		}
		$style = ' width="'.$width.'" height="'.$height.'"';
	}

	if ( ! $alt ) {
		$alt = get_the_title( $attachment_id );
	}

	if ( ! $title ) {
		$title = get_the_title( $attachment_id );
	}

	return '<img src="' . esc_url( $src[0] ) . '" alt="' . esc_attr( $alt ) . '" title="' . esc_attr( $title ) . '" '.$style.'>';

}


/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function thim_event_add_meta_boxes() {

	if ( ! post_type_exists( 'tp_event' ) || ! post_type_exists( 'our_team' ) ) {
		return;
	}
	add_meta_box(
		'thim_organizers',
		esc_html__( 'Organizers', 'eduma' ),
		'thim_event_meta_boxes_callback',
		'tp_event'
	);
}

add_action( 'add_meta_boxes', 'thim_event_add_meta_boxes' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function thim_event_meta_boxes_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'thim_event_save_meta_boxes', 'thim_event_meta_boxes_nonce' );

	// Get all team
	$team = new WP_Query( array(
		'post_type'           => 'our_team',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true,
		'posts_per_page'      => - 1
	) );

	if ( empty( $team->post_count ) ) {
		echo '<p>' . esc_html__( 'No members exists. You can create a member data from', 'eduma' ) . ' <a target="_blank" href="' . admin_url( 'post-new.php?post_type=our_team' ) . '">Our Team</a></p>';

		return;
	}

	echo '<label for="thim_event_members">';
	esc_html_e( 'Get Members', 'eduma' );
	echo '</label> ';
	echo '<select id="thim_event_members" name="thim_event_members[]" multiple>';
	if ( isset( $team->posts ) ) {
		$team = $team->posts;
		foreach ( $team as $member ) {
			echo '<option value="' . esc_attr( $member->ID ) . '">' . get_the_title( $member->ID ) . '</option>';
		}
	}
	echo '</select>';
	echo '<span>';
	esc_html_e( 'Hold down the Ctrl (Windows) / Command (Mac) button to select multiple options.', 'eduma' );
	echo '</span><br>';
	wp_reset_postdata();

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$members = get_post_meta( $post->ID, 'thim_event_members', true );
	echo '<p>' . esc_html__( 'Current Members: ', 'eduma' );
	if ( ! $members ) {
		echo esc_html__( 'None', 'eduma' ) . '</p>';
	} else {
		$total = count( $members );
		foreach ( $members as $key => $id ) {
			echo '<strong><a target="_blank" href="' . get_edit_post_link( $id ) . '">' . get_the_title( $id ) . '</a></strong>';
			if ( $key != count( $total ) ) {
				echo ', ';
			}
		}
	}
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function thim_event_save_meta_boxes( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['thim_event_meta_boxes_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['thim_event_meta_boxes_nonce'], 'thim_event_save_meta_boxes' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'tp_event' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

	}

	/* OK, it's safe for us to save the data now. */

	// Make sure that it is set.
	if ( ! isset( $_POST['thim_event_members'] ) ) {
		return;
	}

	// Update the meta field in the database.
	update_post_meta( $post_id, 'thim_event_members', $_POST['thim_event_members'] );
}

add_action( 'save_post', 'thim_event_save_meta_boxes' );


/**
 * Change default comment fields
 *
 * @param $field
 *
 * @return string
 */
function thim_new_comment_fields( $fields ) {
	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ? 'aria-required=true' : '' );

	$fields = array(
		'author' => '<p class="comment-form-author">' . '<input placeholder="' . esc_attr__( 'Name', 'eduma' ) . ( $req ? ' *' : '' ) . '" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" ' . $aria_req . ' /></p>',
		'email'  => '<p class="comment-form-email">' . '<input placeholder="' . esc_attr__( 'Email', 'eduma' ) . ( $req ? ' *' : '' ) . '" id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" ' . $aria_req . ' /></p>',
		'url'    => '<p class="comment-form-url">' . '<input placeholder="' . esc_attr__( 'Website', 'eduma' ) . '" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
	);

	return $fields;
}

add_filter( 'comment_form_default_fields', 'thim_new_comment_fields', 1 );


/**
 * Remove Emoji scripts
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Optimize script files
 */
function thim_optimize_scripts() {
	global $wp_scripts;
	if ( ! is_a( $wp_scripts, 'WP_Scripts' ) ) {
		return;
	}
	foreach ( $wp_scripts->registered as $handle => $script ) {
		$wp_scripts->registered[ $handle ]->ver = null;
	}
}

add_action( 'wp_print_scripts', 'thim_optimize_scripts', 999 );
add_action( 'wp_print_footer_scripts', 'thim_optimize_scripts', 999 );

/**
 * Optimize style files
 */
function thim_optimize_styles() {
	global $wp_styles;
	if ( ! is_a( $wp_styles, 'WP_Styles' ) ) {
		return;
	}
	foreach ( $wp_styles->registered as $handle => $style ) {
		$wp_styles->registered[ $handle ]->ver = null;
	}
}

add_action( 'admin_print_styles', 'thim_optimize_styles', 999 );
add_action( 'wp_print_styles', 'thim_optimize_styles', 999 );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @param array $args Configuration arguments.
 *
 * @return array
 */
function thim_page_menu_args( $args ) {
	$args['show_home'] = true;

	return $args;
}

add_filter( 'wp_page_menu_args', 'thim_page_menu_args' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function thim_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}

add_filter( 'body_class', 'thim_body_classes' );

/**
 * Sets the authordata global when viewing an author archive.
 *
 * @global WP_Query $wp_query WordPress Query object.
 * @return void
 */
function thim_setup_author() {
	global $wp_query;

	if ( $wp_query->is_author() && isset( $wp_query->post ) ) {
		$GLOBALS['authordata'] = get_userdata( $wp_query->post->post_author );
	}
}

add_action( 'wp', 'thim_setup_author' );

function thim_add_event_admin_styles() {
	?>
	<style type="text/css">
		#thim_event_members {
			min-height: 200px;
		}
	</style>
	<?php
}

add_action( 'admin_print_styles', 'thim_add_event_admin_styles' );

/**
 * Check a plugin activate
 *
 * @param $plugin
 *
 * @return bool
 */
function thim_plugin_active( $plugin ) {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( $plugin ) ) {
		return true;
	}

	return false;
}

/**
 * Get data customizer
 *
 * @return array|void
 */
function thim_options_data() {
	global $theme_options_data;
	$theme_options_data = get_theme_mods();

	return $theme_options_data;
}

/**
 * Custom WooCommerce breadcrumbs
 *
 * @return array
 */
function thim_woocommerce_breadcrumbs() {
	return array(
		'delimiter'   => '',
		'wrap_before' => '<ul class="breadcrumbs" id="breadcrumbs" itemtype="http://schema.org/BreadcrumbList" itemscope="" itemprop="breadcrumb">',
		'wrap_after'  => '</ul>',
		'before'      => '<li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">',
		'after'       => '</li>',
		'home'        => esc_html__( 'Home', 'eduma' ),
	);
}

add_filter( 'woocommerce_breadcrumb_defaults', 'thim_woocommerce_breadcrumbs' );

/**
 * Display post thumbnail by default
 *
 * @param $size
 */
function thim_default_get_post_thumbnail( $size ) {

	if ( thim_plugin_active( 'thim-framework/tp-framework.php' ) ) {
		return;
	}

	if ( get_the_post_thumbnail( get_the_ID(), $size ) ) {
		?>
		<div class='post-formats-wrapper'>
			<a class="post-image" href="<?php echo esc_url( get_permalink() ); ?>">
				<?php echo get_the_post_thumbnail( get_the_ID(), $size ); ?>
			</a>
		</div>
		<?php
	}
}

add_action( 'thim_entry_top', 'thim_default_get_post_thumbnail', 20 );


/**
 * Set unlimit events in archive
 *
 * @param $query
 */
function thim_event_post_filter( $query ) {
	global $wp_query;

	if ( is_post_type_archive( 'tp_event' ) && 'tp_event' == $query->get( 'post_type' ) ) {
		$query->set( 'posts_per_page', - 1 );

		return;
	}
}

add_action( 'pre_get_posts', 'thim_event_post_filter' );


function thim_start_widget_element_content( $content, $panels_data, $post_id ) {
	global $siteorigin_panels_inline_css;

	if ( ! empty( $siteorigin_panels_inline_css[ $post_id ] ) ) {
		$content = '<style scoped>' . ( $siteorigin_panels_inline_css[ $post_id ] ) . '</style>' . $content;
	}

	return $content;
}

remove_action( 'wp_footer', 'siteorigin_panels_print_inline_css' );
add_filter( 'siteorigin_panels_before_content', 'thim_start_widget_element_content', 10, 3 );

//Override ajax-loader contact form
add_filter('wpcf7_ajax_loader', 'thim_wpcf7_ajax_loader');
function thim_wpcf7_ajax_loader () {
	return  get_bloginfo('stylesheet_directory') . '/images/ajax-loader.gif';
}

function thim_ssl_secure_url( $sources) {
	$scheme = parse_url(site_url(), PHP_URL_SCHEME);
	if( 'https' == $scheme ) {
		if (stripos($sources, 'http://') === 0) {
			$sources = 'https' . substr($sources, 4);
		}
		return $sources;
	}

	return $sources;
}
//add_filter('script_loader_src', 'thim_ssl_secure_url');
//add_filter('style_loader_src', 'thim_ssl_secure_url');
add_filter( 'wp_calculate_image_srcset', 'thim_ssl_secure_url' );
add_filter( 'wp_get_attachment_url', 'thim_ssl_secure_url', 1000 );
add_filter( 'image_widget_image_url', 'thim_ssl_secure_url' );


function thim_course_cat_active( $output, $args ) {
	if(is_single()){
		global $post;

		$terms = get_the_terms( $post->ID, $args['taxonomy'] );
		foreach( $terms as $term )
			if ( preg_match( '#cat-item-' . $term ->term_id . '#', $output ) )
				$output = str_replace('cat-item-'.$term ->term_id, 'cat-item-'.$term ->term_id . ' current-cat', $output);
	}
	return $output;
}
//add_filter( 'wp_list_categories', 'thim_course_cat_active', 10, 2 );